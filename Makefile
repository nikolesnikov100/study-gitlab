FLAGS = -O4 -Wall

LIBS = /usr/local/lib/libntl.a -lgmp -pthread

CXX = g++

all: main

main: main.c
	$(CXX) -o main main.c $(FLAGS) $(LIBS) 
	
clean:
	rm -f *.o main

